# UMETRICS Awards Data Documentation

Please see [Wiki](https://bitbucket.org/umetrics/umetrics-awards-iris/wiki/Home) for a list of data enhancement opportunities.

Metadata and summary presentation of the data are available in your browser in the [Documentation](https://bitbucket.org/umetrics/umetrics-awards-iris/addon/documentation/index) section, or as a [pdf file](https://bitbucket.org/umetrics/umetrics-awards-iris/raw/042504a7f5ec27a99fd00399e850f10d6e6e880e/docs/umetrics_documentation.pdf).