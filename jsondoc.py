
class jsondoc:

	"""Creates viewable html documentation from a datapackage.json-type file and inserts it into a jupyter notebook as a markdown cell"""

	css_dict = { 
	R'class' : r'border-collapse:collapse;margin:0px auto;margin-top:5px;margin-bottom:5px',
	R'td' : r'font-family:Arial,sans-serif;font-size:14px;padding:10px 20px;border-style:solid;border-width:1px;',
	R'th' : r'font-family:Arial,sans-serif;font-size:16px;padding:10px 20px;border-style:solid;border-width:1px;',
	R'tr-even' : r'background-color:#F0F0F5',
	R'tr-odd' : r'background-color:#FFFFFF'
	}


	def __init__(self):
		self.data = ''
		self.descriptives = dict()
		self.multiples = list()
		self.schemas = dict()
		self.datastr = ''
		self.db_url = ''
		self.db_name = ''
		self.notebook = ''
		self.variable = ''
		self.cells = list()

	def set(self,nb):
		import nbformat
		import sys
		import os

		# takes a notebook name in the form of a file path with or without '.ipynb' at the end
		if nb[len(nb)-6:] == '.ipynb':
			pass
		else:
			nb = nb + '.ipynb'

		file_exists = os.path.isfile(nb)
		if file_exists:
			# create name for new file.  add sequence number to end of input file name
			self.notebook = self.newname(nb)

			# open and read the existing notebook into a data structure
			with open(nb,'r') as fin:
				nb0 = nbformat.read(fin,4)

			# add the new cell to the end of the existing list of cells
			for k,v in nb0.items():
				if k == 'cells':
					self.cells = v
		else:
			self.notebook = nb


	def summary(self):
		import os

		#-----------------------  upper display  --------------------------------------------------

		self.datastr += "<h1 style='text-align:center'>"+self.descriptives['title']+"</h1>\n"
		self.datastr += "<h2 style='text-align:center'>"+self.descriptives['description']+"</h2>\n"

		for m in self.multiples:
			if m[0:1] == '1':
				self.datastr += "<h3 style='text-align:center'>"+m[2:].capitalize()+"</h3>\n"
			elif m == 'begin_section' : self.datastr += "<table style='"+self.css_dict['class']+"'>\n"
			elif m[0:1] == '2':
				pass
			elif m == 'begin_entry': self.datastr += "<tr>\n"
			elif m == 'end_entry': self.datastr += "</tr>\n"
			elif m[0:1] == '3':
				self.datastr += "<td style="+self.css_dict['td']+">"+m[2:]+"</td>\n"
			elif m == 'end_section' : self.datastr += "</table>\n"

		#-----------------------  variable tables  -----------------------------------------------

		# A schema is a database table. A field is one variable described in one of the dicts listed in a schema.

		self.datastr += "<h3 style='text-align:center'>Database URL: "+self.db_url+"</h3>\n"

		for key,val in self.schemas.items():
			self.datastr += "<h3 style='text-align:center'>Table: "+key+"</h3>\n"
			self.datastr += "<table style='"+self.css_dict['class']+"'>\n<tr style='background-color:#FFCCFF'>\n"
			self.datastr += "<th style='"+self.css_dict['th']+"'>name</th>\n"
			self.datastr += "<th style='"+self.css_dict['th']+"'>title</th>\n"
			self.datastr += "<th style='"+self.css_dict['th']+"'>description</th>\n"
			self.datastr += "<th style='"+self.css_dict['th']+"'>data type</th></tr>\n"

			table_dict = self.schemas[key]
			field_list = table_dict['field']  # list of dicts, one for each variable
			counter = 1
			for vdict in field_list:
				actual_fields = vdict.keys()
				possible_fields = ['title','description','type','format']
				for p in possible_fields:
					if p not in actual_fields: vdict[p] = ' '

				style = self.css_dict['tr-odd']
				counter += 1
				if counter % 2 == 0:
					style = self.css_dict['tr-even']
				self.datastr += "<tr style='"+style+"'>\n"
				self.datastr += "<td style="+self.css_dict['td']+">"+vdict['name']+'</td>\n'
				self.datastr += "<td style="+self.css_dict['td']+">"+vdict['title']+'</td>\n'
				self.datastr += "<td style="+self.css_dict['td']+">"+vdict['description']+'</td>\n'
				self.datastr += "<td style="+self.css_dict['td']+">"+vdict['type']+'</td>\n'
				self.datastr += '</tr>\n'

			self.datastr += '</table>\n'
			self.add_cell()


	def var(self,var):
		# takes variable 'name' as input; i.e., the string in the "name" key of the "field" dict or the
		# first column of the summary cell.
		
		self.variable = var
		for key,val in self.schemas.items():

			table_dict = self.schemas[key]
			field_list = table_dict['field']  # list of dicts, one for each variable
			
			for vdict in field_list:
				if vdict['name'] == self.variable:
					actual_fields = vdict.keys()
					possible_fields = ['name','title','description','quality','type','format']
					for p in possible_fields:
						if p not in actual_fields: vdict[p] = ' ' 

					self.datastr += "<span style='font-size:16px'><em>"+vdict['name']+"</em></span><br />\n"
					self.datastr += '<span style=''font-size:16px;font-weight:bold''>'+vdict['title']+'</span><br />\n'
					if vdict['description'] != ' ':
						self.datastr += "<span style='text-decoration:underline'>Description:</span><br /><span>"+vdict['description']+"</span><br />\n"
					if vdict['quality'] != ' ':
						self.datastr += "<span style='text-decoration:underline'>Quality:</span><br /><span>"+vdict['quality']+"</span><br />\n"
					if vdict['type'] != ' ':
						self.datastr += "<span style='text-decoration:underline'>Type:</span><br /><span>"+vdict['type']+"</span><br />\n"
					if vdict['format'] != ' ':
						self.datastr += "<span style='text-decoration:underline'>Format:</span><br /><span>"+vdict['format']+"</span><br />\n"
					#self.datastr += "<span>"+vdict['example']+"</span><br />\n"

		self.add_cell()


	def load(self,file):
		self.data = file
		import json

		with open(self.data,'r') as f:
			d = json.load(f)

		for k,v in d.items():
        		rubric = ''
        		if k in ('name','title','description'):  
                		# lists of characters needing to be put together as a string

                		for item in d[k]:
                        		rubric += item
                		self.descriptives[k] = rubric

        		elif k in ('sources','contributors'):    
                		# lists of simple dictionaries

                		self.multiples.append('1 '+k)
                		self.multiples.append('begin_section')
                		for item in v:
                        		self.multiples.append('begin_entry')
                        		for ke,va in item.items():
                                		self.multiples.append('2 '+ke)
                                		self.multiples.append('3 '+va)
                        		self.multiples.append('end_entry')
                		self.multiples.append('end_section')

        		elif k == 'resources':
                		# 'resources': list of a single dictionary with 3 keys: name (str), url (str), schemas (dict).
                		# schemas dict consists of one dict for each table, with the table name as they key.
                		# The table dict has three keys: 'description', 'size', and 'field'. The values for 'description' and 'size'
				# are strings. 'field' is the key for a list of dicts, one for each variable. Keys in the 'field'
                		# dicts are 'name','title','description',sometimes 'values', sometimes 'quality','type',
				# sometimes 'format', and sometimes 'example'. The 'values' key is ignored in this code.

                		self.db_name = v[0]['name']
                		self.db_url = v[0]['url']
                		self.schemas = v[0]['schemas']


	def newname(self,name):
		import re
		import os

		idx = name.find('.')
		newname = name[0:idx]

		m = re.search('_(\d+)$',newname) 

		if m:
			seq = int(m.group(1))
			seq += 1
			idx = newname.find("_")
			newname = newname[0:idx+1] + str(seq) + ".ipynb"
		else:
			newname += "_1.ipynb"

		# Now check if there is already a file by that new name.
		file_exists = os.path.isfile(newname)
		if file_exists:
			newname = self.newname(newname)

		return newname


	def add_cell(self):
		from nbformat.v4.nbbase import new_markdown_cell

		self.cells.append(new_markdown_cell( source=self.datastr ))
		self.datastr = ''


	def finish(self):
		import nbformat
		from nbformat.v4.nbbase import new_notebook

		# open and write a new notebook file
		nb1 = new_notebook( cells=self.cells,metadata={'language': 'python',} )

		with open(self.notebook, 'w') as fout:
			nbformat.write(nb1, fout, 4)

		print(self.notebook,'created.')

