## Public Award Data

### NSF/NIH

Linking National Science Foundation and National Institutes of Health data to obtain additional information on data created at time of award (funding department, PI name, project title and abstract).

#### NSF AWARDS

The National Science Foundation funds research and education in science and engineering, through grants, contracts, and cooperative agreements.  The Foundation accounts for 20% of federal support to academic institutions for basic research. Information about research projects that NSF has funded since 1989 can be found by searching the Award Abstracts Database ([*www.nsf.gov/awards/about.jsp*](http://www.nsf.gov/awards/about.jsp)).  The information includes abstracts that describe the research, and names of principal investigators and their institutions. The database includes both completed and in-process research.

Data were collected, parsed and made available in the AIR UMETRICS Remote Server. These data were collected from XML files posted by NSF at
[http://www.nsf.gov/awardsearch/download.jsp](http://www.nsf.gov/awardsearch/download.jsp).

#### NIH AWARDS

The National Institutes of Health provides access to RePORTER (NIH project search tool) and ExPORTER (NIH Project XML and CSV raw files) that include information on research projects funded by the National Institutes for Health, the Centers for Disease Control and Prevention (CDC), Agency for Healthcare Research and Quality (AHRQ), Health Resources and Services Administration (HRSA), Substance Abuse and Mental Health Services Administration (SAMHSA), and the Department of Veterans Affairs (VA), as well as publications and patents citing suppport from these projects. The data are separated into three major categories: projects, publications and patents. There are also link tables that can be used to establish the many-to-many relationships between projects and their publications. To keep the project files to a manageable size, abstracts are stored on their own in a separate file.

Data were collected, parsed and made available in the AIR UMETRICS Remote Server. These data were collected from CSV files posted by NIH at [*http://exporter.nih.gov/*](http://exporter.nih.gov/).

*Data Dictionary*

The NSF & NIH data dictionaries are a complete listing of all tables, variables, and data descriptions that are collected and maintained in the AIR UMETRICS Remote Desktop Server. The structure of the tables are specific to AIR and are modified for researcher use from the original raw data downloads.

[Link to Data Dictionary (NSF)](../data-dictionary-pdfs/NSF-Data-Dictionary.pdf)
[Link to Data Dictionary (NIH)](../data-dictionary-pdfs/NIH-Data-Dictionary.pdf)

*Record Counts & Summary Statistics*

The link to the excel file includes summary stats for the following:

1.  Total number of unique projects by year by university

2.  Percentage of NIH/NSF coverage by unique numbers with a
    > corresponding award id by university

3.  Merge report:

    a.  by university, how many unique award numbers have a 1:1 match
        > with an award id

    b.  by university, how many unique award numbers have a 1:m match
        > with an award id

    c.  by university, how many unique award numbers share an award id
        > (m:1 or m:m)

4.  Merge report: how many award ids merge to the NIH/NSF data

*Linking to the UMETRICS Data*

The AIR team used the NIH and NSF public data to augment the UMETRICS data by established techniques. A technical description of the linking methodology (longest common substring): 

[Technical Documentation SM to NSF & NIH](nsf-nih-sm-match.pdf) 

**Known Issues**

It is important to note that linking administrative data to public grant data can sometimes be problematic. Universities keep track of award identifiers but do not necessarily contain all elements of the unique identifier that enable precise linking. There are two known issues that make using the public award links somewhat difficult:

1)  Many unique award numbers can be linked to one award ID.
    > Illustration of that below:

2)  Many award numbers can be linked to one (or many) unique
    > award numbers. Illustration of that below:

